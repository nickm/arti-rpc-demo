#!/usr/bin/env python3

import json
import asyncio
import os
import logging

class MethodError(Exception):
    pass

class Connection:
    def __init__(self, path=None):
        if path == None:
            path = os.path.expanduser("~/.local/run/arti/SOCKET")

        self._next_id = 1
        self._path = path
        self._conn = None
        self._pending = {}

    async def connect(self):
        self._r, self._w = await asyncio.open_unix_connection(self._path)
        self._reactor = asyncio.create_task(self._react())

    def _write(self, msg):
        if isinstance(msg, str):
            msg = str.encode('utf-8')
        elif isinstance(msg, bytes):
            pass
        else:
            msg = json.dumps(msg).encode('utf-8')

        logging.debug(f">>> {msg}")
        self._w.write(msg)

    async def _read(self):
        line = await self._r.readline()
        logging.debug(f"<<< {line}")
        msg = json.loads(line)
        return msg

    async def _react(self):
        while True:
            msg = await self._read()
            try:
                request_id = msg["id"]
            except KeyError:
                logging.warn("message {msg} with no id. Shutting down.")
                return

            is_update = "update" in msg

            try:
                if is_update:
                    queue = self._pending[request_id]
                else:
                    queue = self._pending.pop(request_id)
            except KeyError:
                logging.warn("message {msg} had id with no pending request. Dropping it.")
                continue

            await queue.put(msg)


    def _run_method_inner(self, queue,  obj_id, method, params={}, accept_updates=False):
        request_id = self._next_id
        self._next_id += 1

        if accept_updates:
            maxsize = 0
        else:
            maxsize = 3
        self._pending[request_id] = queue

        msg = {
            "id": request_id,
            "obj": obj_id,
            "method": method,
            "params": params,
        }
        if accept_updates:
            msg["meta"] = { "updates": True }

        self._write(msg)

    async def run_method(self, obj_id, method, **params):
        queue = asyncio.Queue(1)

        self._run_method_inner(queue, obj_id, method, params, accept_updates=False)

        answer = await queue.get()
        queue.task_done() # yuck.
        if "error" in answer:
            raise MethodError(answer["error"])
        elif "result" in answer:
            return answer["result"]
        else:
            logging.warn("Impossible return from run_method!")

    async def run_method_with_updates(self, obj_id, method, params={}):
        pass

async def run():
    conn = Connection()
    await conn.connect()
    res = await conn.run_method("connection", "auth:query")
    if "inherent:unix_path" not in res['schemes']:
        logging.warn("oops, no recognized scheme.")
        return
    session = await conn.run_method("connection", "auth:authenticate", scheme='inherent:unix_path')
    session = session["session"]
    print(await conn.run_method(session, "arti:x-echo", msg="hello world"))
    


logging.basicConfig(encoding='utf-8', level=logging.DEBUG)
asyncio.run(run())

